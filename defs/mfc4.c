// #define _AFXDLL
// #define _AFX_NO_DOCOBJECT_SUPPORT
#define _AFX_NO_NESTED_DERIVATION
// #define _AFX_NO_OCC_SUPPORT
// #define _AFX_NO_OLE_SUPPORT
// #define _AFX_OLD_EXCEPTIONS
// #define _DEBUG
// #define _MAC

// Forward Declarations
struct _AFX_OCC_DIALOG_INFO; // not defined
struct AFX_CMDHANDLERINFO; // not defined
struct AFX_CONNECTIONMAP; // not defined
struct AFX_DISPMAP; // not defined
struct AFX_EVENTSINK_MAP; // not defined
struct AFX_INTERFACEMAP; // not defined
struct AFX_MSGMAP; struct AFX_MSGMAP_ENTRY;
struct AFX_OLECMDMAP; // not defined
struct CArchive;
struct CCommandLineInfo; // not defined
struct CCreateContext; // not defined;
struct CDataExchange; // not defined
struct CDocument; // not defined
struct CDocManager; // not defined
struct CDocTemplate; // not defined
struct CDumpContext;
struct CFileStatus;
struct CNoTrackObject; struct CNoTrackObject_vtbl;
    struct _AFX_SOCK_STATE; struct _AFX_SOCK_STATE_vtbl; struct _AFX_SOCK_STATE_mbrs;
    struct AFX_MODULE_STATE; struct AFX_MODULE_STATE_vtbl; struct AFX_MODULE_STATE_mbrs;
struct CObject; struct CObject_vtbl; struct CObject_mbrs;
    struct CAsyncSocket; struct CAsyncSocket_vtbl; struct CAsyncSocket_mbrs;
        struct CSocket; struct CSocket_vtbl; struct CSocket_mbrs;
    struct CCmdTarget; struct CCmdTarget_vtbl; struct CCmdTarget_mbrs; struct CCmdTarget::XConnPtContainer; struct CCmdTarget::XDispatch;
        struct CWinThread; struct CWinThread_vtbl; struct CWinThread_mbrs;
            struct CWinApp; struct CWinApp_vtbl; struct CWinApp_mbrs;
        struct CWnd; struct CWnd_vtbl; struct CWnd_mbrs;
            struct CButton; struct CButton_vtbl; struct CButton_mbrs;
            struct CComboBox; struct CComboBox_vtbl; struct CComboBox_mbrs;
            struct CDialog; struct CDialog_vtbl; struct CDialog_mbrs;
            struct CEdit; struct CEdit_vtbl; struct CEdit_mbrs;
            struct CListBox; struct CListBox_vtbl; struct CListBox_mbrs;
            struct CListCtrl; struct CListCtrl_vtbl; struct CListCtrl_mbrs;
            struct CStatic; struct CStatic_vtbl; struct CStatic_mbrs;
            struct CTabCtrl; struct CTabCtrl_vtbl; struct CTabCtrl_mbrs;
            struct CTreeCtrl; struct CTreeCtrl_vtbl; struct CTreeCtrl_mbrs;
    struct CDocManager; struct CDocManager_vtbl; struct CDocManager_mbrs;
    struct CException; struct CException_vtbl; struct CException_mbrs;
        struct CFileException; struct CFileException_vtbl; struct CFileException_mbrs;
    struct CFile; struct CFile_vtbl; struct CFile_mbrs;
        struct CSocketFile; struct CSocketFile_vtbl; struct CSocketFile_mbrs;
    struct CGdiObject; struct CGdiObject_vtbl; struct CGdiObject_mbrs;
        struct CBitmap; struct CBitmap_vtbl; struct CBitmap_mbrs;
        struct CBrush; struct CBrush_vtbl; struct CBrush_mbrs;
        struct CFont; struct CFont_vtbl; struct CFont_mbrs;
        struct CPalette; struct CPalette_vtbl; struct CPalette_mbrs;
    struct CImageList; struct CImageList_vtbl; struct CImageList_mbrs;
    struct CMapPtrToPtr; struct CMapPtrToPtr_vtbl; struct CMapPtrToPtr_mbrs; struct CMapPtrToPtr::CAssoc;
    struct CObList; struct CObList_vtbl; struct CObList_mbrs; struct CObList::CNode;
    struct CPtrArray; struct CPtrArray_vtbl; struct CPtrArray_mbrs;
    struct CPtrList; struct CPtrList_vtbl; struct CPtrList_mbrs; struct CPtrList::CNode;
struct COccManager; // not defined
struct COleControlContainer; // not defined
struct COleControlSite; // not defined
struct COleDropTarget; // not defined
struct COleMessageFilter; // not defined
struct COleObjectFactory; // not defined
struct CPoint { tagPOINT tagPOINT; };
struct CRecentFileList; // not defined
struct CRuntimeClass;
struct CScrollBar; // not defined
union CString { LPTSTR m_pchData; };
struct CStringData;
struct CTime { time_t m_time; };
struct CSimpleList;
struct CTypeLibCache;
struct CWinApp; struct CWinApp_vtbl; struct CWinApp_mbrs;
struct IConnectionPoint; // not defined
struct ITypeInfo; // not defined
struct ITypeLib; // not defined
struct TOOLINFO; // not defined
struct __POSITION { };
typedef void (*AFX_PMSG)();
typedef UINT (__cdecl *AFX_THREADPROC)(LPVOID);
typedef void (__stdcall *AFX_TERM_PROC)();
typedef long DISPID;
typedef IConnectionPoint *LPCONNECTIONPOINT;
typedef ITypeInfo *LPTYPEINFO;
typedef ITypeLib *LPTYPELIB;
typedef __POSITION* POSITION;

// Declarations

union CArchive::Store
{
	CPtrArray *m_pLoadArray;
	CMapPtrToPtr *m_pStoreMap;
};
struct CArchive
{
	struct CDocument *m_pDocument;
	BOOL m_bForceFlat;
	BOOL m_bDirectBuffer;
	UINT m_nObjectSchema;
	CString m_strFileName;
	BOOL m_bMode;
	BOOL m_bUserBuf;
	int m_nBufSize;
	CFile *m_pFile;
	BYTE *m_lpBufCur;
	BYTE *m_lpBufMax;
	BYTE *m_lpBufStart;
	UINT m_nMapCount;
    CArchive::Store store;
	CMapPtrToPtr *m_pSchemaMap;
	UINT m_nGrowSize;
	UINT m_nHashSize;
};

struct CDumpContext
{
	int m_nDepth;
	CFile *m_pFile;
};

struct CNoTrackObject
{
    CNoTrackObject_vtbl *__vftable;
};
struct CNoTrackObject_vtbl
{
    CNoTrackObject *(__thiscall *vdtor)(CNoTrackObject *this);
};

struct _AFX_SOCK_STATE_vtbl
{
    CNoTrackObject_vtbl CNoTrackObject;
};
struct _AFX_SOCK_STATE_mbrs
{
    DWORD m_dwReserved1;
    HINSTANCE m_hInstSOCK;
    void(__stdcall *m_pfnSockTerm)();
};
struct _AFX_SOCK_STATE
{
    _AFX_SOCK_STATE_vtbl *__vftable;
    _AFX_SOCK_STATE_mbrs __members;
};

struct CSimpleList
{
    void *m_pHead;
    size_t m_nNextOffset;
};

struct CTypeLibCache
{
    LCID m_lcid;
    LPTYPELIB m_ptlib;
    GUID m_guidInfo;
    LPTYPEINFO m_ptinfo;
    long m_cRef;
};

struct AFX_MSGMAP_ENTRY
{
    MACRO_WM nMessage;
    UINT nCode;
    UINT nID;
    UINT nLastID;
    UINT nSig;
    AFX_PMSG pfn;
};
struct AFX_MSGMAP
{
#ifdef _AFXDLL
    const AFX_MSGMAP *(*pfnGetBaseMap)();
#else
    const AFX_MSGMAP *pBaseMap;
#endif
    const AFX_MSGMAP_ENTRY *lpEntries;
};

struct AFX_MODULE_STATE_vtbl
{
    CNoTrackObject_vtbl CNoTrackObject;
};
struct AFX_MODULE_STATE_mbrs
{
    CWinApp *m_pCurrentWinApp;
    HINSTANCE m_hCurrentInstanceHandle;
    HINSTANCE m_hCurrentResourceHandle;
    LPCTSTR m_lpszCurrentAppName;
    BYTE m_bDLL;
    BYTE m_bSystem;
    BYTE m_bReserved[2];
    short m_fRegisteredClasses;
#ifdef _AFXDLL
    CRuntimeClass *m_pClassInit;
#endif
    CSimpleList m_classList; // CTypedSimpleList<CRuntimeClass*>
#ifndef _AFX_NO_OLE_SUPPORT
#ifdef _AFX_DLL
    COleObjectFactory *m_pFactoryInit;
#endif
    CSimpleList m_factoryList; // CTypedSimpleList<ColeObjectFactory*>
    #endif
    long m_nObjectCount;
    BOOL m_bUserCtrl;
    TCHAR m_szUnregisterList[4096];
#ifdef _AFXDLL
    WNDPROC m_pfnAfxWndProc;
    DWORD m_dwVersion;
#endif
#ifdef _AFX_OLD_EXCEPTIONS
    AFX_TERM_PROC m_pfnTerminate;
#endif
    void(__stdcall *m_pfnFilterToolTipMessage)(MSG*, CWnd*);
#ifdef _AFXDLL
    CSimpleList m_libraryList; // CTypedSimpleList<CDynLinkLibrary*>
    HINSTANCE m_appLangDLL;
#endif
#ifndef _AFX_NO_OCC_SUPPORT
    COccManager *m_pOccManager;
    CSimpleList m_lockList; // CTypedSimpleList<COleControlLock*>
#endif
#ifndef _AFX_NO_DAO_SUPPORT
    CTypeLibCache m_typeLibCache;
    CMapPtrToPtr *m_pTypeLibCacheMap;
#endif
};
struct AFX_MODULE_STATE
{
    AFX_MODULE_STATE_vtbl *__vftable;
    AFX_MODULE_STATE_mbrs __members;
};

struct CObject_vtbl
{
	CRuntimeClass *(__thiscall *GetRuntimeClass)(const CObject *this);
	CObject *(__thiscall *vdtor)(CObject *this);
	void (__thiscall *Serialize)(CObject *this, CArchive *ar);
	void (__thiscall *AssertValid)(const CObject *this);
	void (__thiscall *Dump)(const CObject *this, CDumpContext *dc);
};
struct CObject
{
	CObject_vtbl *__vftable;
};

struct CAsyncSocket_vtbl
{
    CObject_vtbl CObject;
    BOOL (__thiscall *Accept)(CAsyncSocket *this, CAsyncSocket *rConnectedSocked, SOCKADDR *lpSockADdr, int *lpSockAddrLen);
    void (__thiscall *Close)(CAsyncSocket *this);
    int (__thiscall *Receive)(CAsyncSocket *this, void *lpBuf, int nBufLen, int nFlags);
    int (__thiscall *Send)(CAsyncSocket *this, const void *lpBuf, int nBufLen, int nFlags);
    void (__thiscall *OnReceive)(CAsyncSocket *this, int nErrorCode);
    void (__thiscall *OnSend)(CAsyncSocket *this, int nErrorCode);
    void (__thiscall *OnOutOfBoundData)(CAsyncSocket *this, int nErrorCode);
    void (__thiscall *OnAccept)(CAsyncSocket *this, int nErrorCode);
    void (__thiscall *OnConnect)(CAsyncSocket *this, int nErrorCode);
    void (__thiscall *OnClose)(CAsyncSocket *this, int nErrorCode);
    BOOL (__thiscall *ConnectHelper)(CAsyncSocket *this, const SOCKADDR *lpSockAddr, int nSockAddrLen);
    int (__thiscall *ReceiveFromHelper)(CAsyncSocket *this, void *lpBuf, int nBufLen, SOCKADDR *lpSockAddr, int *lpSockAddrLen, int nFlags);
    int (__thiscall *SendToHelper)(CAsyncSocket *this, const void *lpBuf, int nBufLen, const SOCKADDR *lpSockAddr, int nSockAddrLen, int nFlags);
};
struct CAsyncSocket_mbrs
{
    SOCKET m_hSocket;
};
struct CAsyncSocket
{
    CAsyncSocket_vtbl *__vftable;
    CAsyncSocket_mbrs __members;
};

struct CSocket_vtbl
{
    CAsyncSocket_vtbl CAsyncSocket;
    BOOL (__thiscall *OnMessagePending)(CSocket *this);
    BOOL (__thiscall *PumpMessages)(CSocket *this, UINT uStopFlag);
};
struct CSocket_mbrs
{
    CAsyncSocket_mbrs CAsyncSocket;
    int m_nTimeOut;
    BOOL *m_pbBlocking;
    int m_nConnectError;
};
struct CSocket
{
    CSocket_vtbl *__vftable;
    CSocket_mbrs __members;
};

struct CCmdTarget::XDispatch
{
    DWORD m_vtbl;
#ifndef _AFX_NO_NESTED_DERIVATION
    size_t m_nOffset;
#endif
};
struct CCmdTarget::XConnPtContainer
{
    DWORD m_vtbl;
#ifndef _AFX_NO_NESTED_DERIVATION
    size_t m_nOffset;
#endif
};
struct CCmdTarget_vtbl
{
    CObject_vtbl CObject;
    BOOL (__thiscall *OnCmdMsg)(CCmdTarget *this, UINT nID, int nCode, void *pExtra, struct AFX_CMDHANDLERINFO *pHandlerInfo);
#ifndef _AFX_NO_OLE_SUPPORT
    void (__thiscall *OnFinalRelease)(CCmdTarget *this);
    BOOL (__thiscall *IsInvokeAllowed)(CCmdTarget *this, DISPID dispid);
    BOOL (__thiscall *GetDispatchIID)(CCmdTarget *this, IID* pIID);
    UINT (__thiscall *GetTypeInfoCount)(CCmdTarget *this);
    struct CTypeLibCache *(__thiscall *GetTypeLibCache)(CCmdTarget *this);
    HRESULT (__thiscall *GetTypeLib)(CCmdTarget *this, LCID lcid, LPTYPELIB* ppTypeLib);
    const AFX_MSGMAP *(__thiscall *GetMessageMap)(const CCmdTarget *this);
#endif
#ifndef _AFX_NO_DOCOBJECT_SUPPORT
    const AFX_OLECMDMAP *(__thiscall *GetCommandMap)(const CCmdTarget *this);
#endif
#ifndef _AFX_NO_OLE_SUPPORT
    const AFX_DISPMAP *(__thiscall *GetDispatchMap)(const CCmdTarget *this);
    const AFX_CONNECTIONMAP *(__thiscall *GetConnectionMap)(const CCmdTarget *this);
    const AFX_INTERFACEMAP *(__thiscall *GetInterfaceMap)(const CCmdTarget *this);
#ifndef _AFX_NO_OCC_SUPPORT
    const AFX_EVENTSINK_MAP *(__thiscall *GetEventSinkMap)(const CCmdTarget *this);
#endif
    BOOL (__thiscall *OnCreateAggregates)(CCmdTarget *this);
    LPUNKNOWN (__thiscall *GetInterfaceHook)(CCmdTarget *this, const void*);
    BOOL (__thiscall *GetExtraConnectionPoints)(CCmdTarget *this, CPtrArray *pConnPoints);
    LPCONNECTIONPOINT (__thiscall *GetConnectionHook)(CCmdTarget *this, const IID* iid);
#endif
};
struct CCmdTarget_mbrs
{
    long m_dwRef;
    LPUNKNOWN m_pOuterUnknown;
    DWORD m_xInnerUnknown;
    CCmdTarget::XDispatch m_xDispatch;
    BOOL m_bResultExpected;
    CCmdTarget::XConnPtContainer m_xConnPtContainer;
#ifdef _AFXDLL
    AFX_MODULE_STATE *m_pModuleState;
#endif
};
struct CCmdTarget
{
    CCmdTarget_vtbl *__vftable;
    CCmdTarget_mbrs __members;
};

struct CWinThread_vtbl
{
    CCmdTarget_vtbl CCmdTarget;
    BOOL (__thiscall *InitInstance)(CWinThread *this);
    int (__thiscall *Run)(CWinThread *this);
    BOOL (__thiscall *PreTranslateMessage)(CWinThread *this, MSG *pMsg);
    BOOL (__thiscall *PumpMessage)(CWinThread *this);
    BOOL (__thiscall *OnIdle)(CWinThread *this, LONG lCount);
    BOOL (__thiscall *IsIdleMessage)(CWinThread *this, MSG* pMsg);
    int (__thiscall *ExitInstance)(CWinThread *this);
    LRESULT (__thiscall *ProcessWndProcException)(CWinThread *this, CException *e, const MSG *pMsg);
    BOOL (__thiscall *ProcessMessageFilter)(CWinThread *this, int code, LPMSG lpMsg);
    CWnd *(__thiscall *GetMainWnd)(CWinThread *this);
    void (__thiscall *Delete)(CWinThread *this);
};
struct CWinThread_mbrs
{
    CCmdTarget_mbrs CCmdTarget;
    CWnd *m_pMainWnd;
    CWnd *m_pActiveWnd;
    BOOL m_bAutoDelete;
    HANDLE m_hThread;
    DWORD m_nThreadID;
#ifdef _DEBUG
    int m_nDisablePumpCount;
#endif
    MSG m_msgCur;
    LPVOID m_pThreadParams;
    AFX_THREADPROC m_pfnThreadProc;
    void(__stdcall *m_lpfnOleTermOrFreeLib)(BOOL, BOOL);
    COleMessageFilter *m_pMessageFilter;
    CPoint m_ptCursorLast;
    UINT m_nMsgLast;
};
struct CWinThread
{
    CWinThread_vtbl *__vftable;
    CWinThread_mbrs __members;
};

#ifdef _MAC
enum CWinApp::SaveOption
{
    saveYes,
    saveNo,
    saveAsk
};
#endif
struct CWinApp_vtbl
{
    CWinThread_vtbl CWinThread;
    CDocument *(__thiscall *OpenDocumentFile)(CWinApp *this, LPCTSTR lpszFileName);
    void (__thiscall *AddToRecentFileList)(CWinApp *this, LPCTSTR lpszPathName);
    BOOL (__thiscall *InitApplication)(CWinApp *this);
#ifdef _MAC
    BOOL (__thiscall *CreateInitialDocument)(CWinApp *this);
#endif
    BOOL (__thiscall *SaveAllModified)(CWinApp *this);
    int (__thiscall *DoMessageBox)(CWinApp *this, LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt);
    void (__thiscall *DoWaitCursor)(CWinApp *this, int nCode);
#ifndef _MAC
    BOOL (__thiscall *OnDDECommand)(CWinApp *this, LPTSTR lpszCommand);
#endif
    void (__thiscall *WinHelp)(CWinApp *this, DWORD dwData, UINT nCmd);
};
struct CWinApp_mbrs
{
    CWinThread_mbrs CWinThread;
    HINSTANCE m_hInstance;
    HINSTANCE m_hPrevInstance;
    LPTSTR m_lpCmdLine;
    int m_nCmdSHow;
    LPCTSTR m_pszAppName;
    LPCTSTR m_pszRegistryKey;
    CDocManager *m_pDocManager;
    BOOL m_bHelpMode;
#ifdef _MAC
    CWinApp::SaveOption m_nSaveOption;
#endif
    LPCTSTR m_pszExeName;
    LPCTSTR m_pszHelpFilePath;
    LPCTSTR m_pszProfileName;
    HGLOBAL m_hDevMode;
    HGLOBAL m_hDevNames;
    DWORD m_dwPromptContext;
    int m_nWaitCursorCount;
    HCURSOR m_hcurWaitCursorRestore;
    CRecentFileList *m_pRecentFileList;
    CCommandLineInfo *m_pCmdInfo;
    ATOM m_atomApp;
    ATOM m_atomSystemTopic;
    UINT m_nNumPreviewPages;
    size_t m_nSafetyPoolSize;
    void (__stdcall *m_lpfnDaoTerm)();
};
struct CWinApp
{
    CWinApp_vtbl *__vftable;
    CWinApp_mbrs __members;
};

struct CWnd_vtbl
{
    CCmdTarget_vtbl CCmdTarget;
    void (__thiscall *PreSubclassWindow)(CWnd *this);
    BOOL (__thiscall *Create)(CWnd *this, LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT *rect, CWnd *pParentWnd, UINT nID, CCreateContext *pContext);
    BOOL (__thiscall *DestroyWindow)(CWnd *this);
    BOOL (__thiscall *PreCreateWindow)(CWnd *this, CREATESTRUCT *cs);
    void (__thiscall *CalcWindowRect)(CWnd *this, LPRECT lpClientRect, UINT nAdjustType);
    int (__thiscall *OnToolHitTest)(const CWnd *this, CPoint point, TOOLINFO *pTI);
    CScrollBar *(__thiscall *GetScrollBarCtrl)(const CWnd *this, int nBar);
    void (__thiscall *WinHelp)(CWnd *this, DWORD dwData, UINT nCmd);
    BOOL (__thiscall *ContinueModal)(CWnd *this);
    void (__thiscall *EndModalLoop)(CWnd *this, int nResult);
    BOOL (__thiscall *OnCommand)(CWnd *this, WPARAM wParam, LPARAM lParam);
    BOOL (__thiscall *OnNotify)(CWnd *this, WPARAM wParam, LPARAM lParam, LRESULT *pResult);
    WNDPROC *(__thiscall *GetSuperWndProcAddr)(CWnd *this);
    void (__thiscall *DoDataExchange)(CWnd *this, CDataExchange *pDX);
    void (__thiscall *BeginModalState)(CWnd *this);
    void (__thiscall *EndModalState)(CWnd *this);
    BOOL (__thiscall *PreTranslateMessage)(CWnd *this, MSG *pMsg);
#ifndef _AFX_NO_OCC_SUPPORT
    BOOL (__thiscall *OnAmbientProperty)(CWnd *this, COleControlSite *pSite, DISPID dispid, VARIANT *pvar);
#endif
    LRESULT (__thiscall *WindowProc)(CWnd *this, UINT message, WPARAM wParam, LPARAM lParam);
    BOOL (__thiscall *OnWndMsg)(CWnd *this, UINT message, WPARAM wParam, LPARAM lParam, LRESULT *pResult);
    LRESULT (__thiscall *DefWindowProc)(CWnd *this, UINT message, WPARAM wParam, LPARAM lParam);
    void (__thiscall *PostNcDestroy)(CWnd *this);
    BOOL (__thiscall *OnChildNotify)(CWnd *this, UINT message, WPARAM wParam, LPARAM lParam, LRESULT *pResult);
    BOOL (__thiscall *CheckAutoCenter)(CWnd *this);
    BOOL (__thiscall *IsFrameWnd)(const CWnd *this);
    void (__thiscall *OnFinalRelease)(CWnd *this);
#ifndef _AFX_NO_OCC_SUPPORT
    BOOL (__thiscall *SetOccDialogInfo)(CWnd *this, struct _AFX_OCC_DIALOG_INFO *pOccDialogInfo);
#endif
};
struct CWnd_mbrs
{
    CCmdTarget_mbrs CCmdTarget;
    HWND m_hWnd;
    HWND m_hWndOwner;
    UINT m_nFlags;
    WNDPROC m_pfnSuper;
    int m_nModalResult;
    COleDropTarget *m_pDropTarget;
#ifndef _AFX_NO_OCC_SUPPORT
    COleControlContainer *m_pCtrlCont;
    COleControlSite *m_pCtrlSite;
#endif
};
struct CWnd
{
    CWnd_vtbl *__vftable;
    CWnd_mbrs __members;
};

struct CButton_vtbl
{
    CWnd_vtbl CWnd;
    void (__thiscall *DrawItem)(CButton *this, LPDRAWITEMSTRUCT lpDrawItemStruct);
};
struct CButton_mbrs
{
    CWnd_mbrs CWnd;
};
struct CButton
{
    CButton_vtbl *__vftable;
    CButton_mbrs __members;
};

struct CComboBox_vtbl
{
    CWnd_vtbl CWnd;
    void (__thiscall *DrawItem)(CComboBox *this, LPDRAWITEMSTRUCT lpDrawItemStruct);
    void (__thiscall *MeasureItem)(CComboBox *this, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    int (__thiscall *CompareItem)(CComboBox *this, LPCOMPAREITEMSTRUCT lpCompareItemStruct);
    void (__thiscall *DeleteItem)(CComboBox *this, LPDELETEITEMSTRUCT lpDeleteItemStruct);
};
struct CComboBox_mbrs
{
    CWnd_mbrs CWnd;
};
struct CComboBox
{
    CComboBox_vtbl *__vftable;
    CComboBox_mbrs __members;
};

struct CDialog_vtbl
{
    CWnd_vtbl CWnd;
    int (__thiscall *DoModal)(CDialog *this);
    BOOL (__thiscall *OnInitDialog)(CDialog *this);
    void (__thiscall *OnSetFont)(CDialog *this, CFont *pFont);
    void (__thiscall *OnOK)(CDialog *this);
    void (__thiscall *OnCancel)(CDialog *this);
    void (__thiscall *PreInitDialog)(CDialog *this);
};
struct CDialog_mbrs
{
    CWnd_mbrs CWnd;
    UINT m_nIDHelp;
    LPCTSTR m_lpszTemplateName;
    HGLOBAL m_hDialogTemplate;
    LPCDLGTEMPLATE m_lpDialogTemplate;
    void *m_lpDialogInit;
    CWnd *m_pParentWnd;
    HWND m_hWndTop;
#ifndef _AFX_NO_OCC_SUPPORT
    struct _AFX_OCC_DIALOG_INFO *m_pOccDialogInfo;
#endif
};
struct CDialog
{
    CDialog_vtbl *__vftable;
    CDialog_mbrs __members;
};

struct CEdit_vtbl
{
    CWnd_vtbl CWnd;
};
struct CEdit_mbrs
{
    CWnd_mbrs CWnd;
};
struct CEdit
{
    CEdit_vtbl *__vftable;
    CEdit_mbrs __members;
};

struct CListBox_vtbl
{
    CWnd_vtbl CWnd;
    void (__thiscall *DrawItem)(CListBox *this, LPDRAWITEMSTRUCT lpDrawItemStruct);
    void (__thiscall *MeasureItem)(CListBox *this, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    int (__thiscall *CompareItem)(CListBox *this, LPCOMPAREITEMSTRUCT lpCompareItemStruct);
    void (__thiscall *DeleteItem)(CListBox *this, LPDELETEITEMSTRUCT lpDeleteItemStruct);
    int (__thiscall *VKeyToItem)(CListBox *this, UINT nKey, UINT nIndex);
    int (__thiscall *CharToItem)(CListBox *this, UINT nKey, UINT nIndex);
};
struct CListBox_mbrs
{
    CWnd_mbrs CWnd;
};
struct CListBox
{
    CListBox_vtbl *__vftable;
    CListBox_mbrs __members;
};

struct CListCtrl_vtbl
{
    CWnd_vtbl CWnd;
};
struct CListCtrl_mbrs
{
    CWnd_mbrs CWnd;
};
struct CListCtrl
{
    CListCtrl_vtbl *__vftable;
    CListCtrl_mbrs __members;
};

struct CStatic_vtbl
{
    CWnd_vtbl CWnd;
};
struct CStatic_mbrs
{
    CWnd_mbrs CWnd;
};
struct CStatic
{
    CStatic_vtbl *__vftable;
    CStatic_mbrs __members;
};

struct CTabCtrl_vtbl
{
    CWnd_vtbl CWnd;
    void (__thiscall *DrawItem)(CTabCtrl *this, LPDRAWITEMSTRUCT lpDrawItemStruct);
};
struct CTabCtrl_mbrs
{
    CWnd_mbrs CWnd;
};
struct CTabCtrl
{
    CTabCtrl_vtbl *__vftable;
    CTabCtrl_mbrs __members;
};

struct CTreeCtrl_vtbl
{
    CWnd_vtbl CWnd;
};
struct CTreeCtrl_mbrs
{
    CWnd_mbrs CWnd;
};
struct CTreeCtrl
{
    CTreeCtrl_vtbl *__vftable;
    CTreeCtrl_mbrs __members;
};

struct CPtrList_vtbl
{
    CObject_vtbl CObject;
};
struct CPtrList_mbrs
{
    CPtrList::CNode *m_pNodeHead;
    CPtrList::CNode *m_pNodeTail;
    int m_nCount;
    CPtrList::CNode *m_pNodeFree;
    struct CPlex *m_pBlocks;
    int m_nBlockSize;
};
struct CPtrList
{
    CPtrList_vtbl *__vftable;
    CPtrList_mbrs __members;
};
struct CPtrList::CNode
{
    CPtrList::CNode *pNext;
    CPtrList::CNode *pPrev;
    void *data;
};

struct CDocManager_vtbl
{
    CObject_vtbl CObject;
    void (__thiscall *AddDocTemplate)(CDocManager *this, CDocTemplate *pTemplate);
    POSITION (__thiscall *GetFirstDocTemplatePosition)(const CDocManager *this);
    CDocTemplate *(__thiscall *GetNextDocTemplate)(const CDocManager *this, POSITION *pos);
    void (__thiscall *RegisterShellFileTypes)(CDocManager *this, BOOL bCompat);
    CDocument *(__thiscall *OpenDocumentFile)(CDocManager *this, LPCTSTR lpszFileName);
    BOOL (__thiscall *SaveAllModified)(CDocManager *this);
    void (__thiscall *CloseAllDocuments)(CDocManager *this, BOOL bEndSession);
    int (__thiscall *GetOpenDocumentCount)(CDocManager *this);
    BOOL (__thiscall *DoPromptFileName)(CDocManager *this, CString *fileName, UINT nIDSTitle, DWORD lFlags, BOOL bOpenFileDialog, CDocTemplate *pTemplate);
#ifndef _MAC
    BOOL (__thiscall *OnDDECommand)(CDocManager *this, LPTSTR lpszCommand);
#endif
    void (__thiscall *OnFileNew)(CDocManager *this);
    void (__thiscall *OnFileOpen)(CDocManager *this);
};
struct CDocManager_mbrs
{
    CPtrList m_templateList;
};
struct CDocManager
{
    CDocManager_vtbl *__vftable;
    CDocManager_mbrs __members;
};

struct CException_vtbl
{
    CObject_vtbl CObject;
    BOOL (__thiscall *GetErrorMessage)(CException *this, LPTSTR lpszError, UINT nMaxError, PUINT pnHelpContext);
    int (__thiscall *ReportError)(CException *this, UINT nType, UINT nMessageID);
};
struct CException_mbrs
{
    BOOL m_bAutoDelete;
#ifdef _DEBUG
    BOOL m_bReadyForDelete;
#endif
};
struct CException
{
    CException_vtbl *__vftable;
    CException_mbrs __members;
};

struct CFileException_vtbl
{
    CException_vtbl CException;
};
struct CFileException_mbrs
{
    CException_mbrs CException;
    int m_cause;
    LONG m_lOsError;
    CString m_strFileName;
};
struct CFileException
{
    CFileException_vtbl *__vftable;
    CFileException_mbrs __members;
};

struct CFileStatus
{
    CTime m_ctime;
    CTime m_mtime;
    CTime m_atime;
    LONG m_size;
    BYTE m_attribute;
    BYTE _m_padding;
    TCHAR m_szFullName[260];
};

struct CFile_vtbl
{
	CObject_vtbl CObject;
	DWORD (__thiscall *GetPosition)(const CFile *this);
	BOOL (__thiscall *GetStatus)(const CFile *this, CFileStatus *rStatus);
    CString (__thiscall *GetFileName)(const CFile *this);
    CString (__thiscall *GetFileTitle)(const CFile *this);
    CString (__thiscall *GetFilePath)(const CFile *this);
    void (__thiscall *SetFilePath)( CFile *this, LPCTSTR lpszNewName);
    BOOL (__thiscall *Open)(CFile *this, LPCTSTR lpszFileName, UINT nOpenFlags, CFileException *pError);
    CFile *(__thiscall *Duplicate)(const CFile *this);
    LONG (__thiscall *Seek)(CFile *this, LONG lOff, UINT nFrom);
    void (__thiscall *SetLength)(CFile *this, DWORD dwNewLen);
    DWORD (__thiscall *GetLength)(const CFile *this);
    UINT (__thiscall *Read)(CFile *this, void *lpBuf, UINT nCount);
    void (__thiscall *Write)(CFile *this, const void *lpBuf, UINT nCount);
    void (__thiscall *LockRange)(CFile *this, DWORD dwPos, DWORD dwCount);
    void (__thiscall *UnlockRange)(CFile *this, DWORD dwPos, DWORD dwCount);
    void (__thiscall *Abort)(CFile *this);
    void (__thiscall *Flush)(CFile *this);
    void (__thiscall *Close)(CFile *this);
    UINT (__thiscall *GetBufferPtr)(CFile *this, UINT nCommand, UINT nCount, void **pBufStart, void **ppBufMax);
};
struct CFile_mbrs
{
	UINT m_hFile;
    BOOL m_bCloseOnDelete;
    CString m_strFileName;
};
struct CFile
{
	CFile_vtbl *__vftable;
	CFile_mbrs __members;
};

struct CSocketFile_vtbl
{
    CFile_vtbl CFile;
};
struct CSocketFile_mbrs
{
    CFile_mbrs CFile;
    CSocket *m_pSocket;
    BOOL m_bArchiveCompatible;
};
struct CSocketFile
{
    CSocketFile_vtbl *__vftable;
    CSocketFile_mbrs __members;
};

struct CGdiObject_vtbl
{
    CObject_vtbl CObject;
};
struct CGdiObject_mbrs
{
    HGDIOBJ m_hObject;
};
struct CGdiObject
{
    CGdiObject_vtbl *__vftable;
    CGdiObject_mbrs __members;
};

struct CBitmap_vtbl
{
    CGdiObject_vtbl CGdiObject;
};
struct CBitmap_mbrs
{
    CGdiObject_mbrs CGdiObject;
};
struct CBitmap
{
    CBitmap_vtbl *__vftable;
    CBitmap_mbrs __members;
};

struct CBrush_vtbl
{
    CGdiObject_vtbl CGdiObject;
};
struct CBrush_mbrs
{
    CGdiObject_mbrs CGdiObject;
};
struct CBrush
{
    CBrush_vtbl *__vftable;
    CBrush_mbrs __members;
};

struct CFont_vtbl
{
    CGdiObject_vtbl CGdiObject;
};
struct CFont_mbrs
{
    CGdiObject_mbrs CGdiObject;
};
struct CFont
{
    CFont_vtbl *__vftable;
    CFont_mbrs __members;
};

struct CPalette_vtbl
{
    CGdiObject_vtbl CGdiObject;
};
struct CPalette_mbrs
{
    CGdiObject_mbrs CGdiObject;
};
struct CPalette
{
    CPalette_vtbl *__vftable;
    CPalette_mbrs __members;
};

struct CImageList_vtbl
{
    CObject_vtbl CObject;
};
struct CImageList_mbrs
{
    HIMAGELIST m_hImageList;
};
struct CImageList
{
    CImageList_vtbl *__vftable;
    CImageList_mbrs __members;
};

struct CMapPtrToPtr_vtbl
{
    CObject_vtbl CObject;
};
struct CMapPtrToPtr_mbrs
{
    CMapPtrToPtr::CAssoc* *m_pHashTable;
    UINT m_nHashTableSize;
    int m_nCount;
    CMapPtrToPtr::CAssoc *m_pFreeList;
    struct CPlex *m_pBlocks;
    int m_nBlockSize;
};
struct CMapPtrToPtr
{
    CMapPtrToPtr_vtbl *__vftable;
    CMapPtrToPtr_mbrs __members;
};
struct CMapPtrToPtr::CAssoc
{
    CMapPtrToPtr::CAssoc *pNext;
    void *key;
    void *value;
};

struct CObList::CNode
{
    CObList::CNode *pNext;
    CObList::CNode *pPrev;
    CObject *data;
};
struct CObList_vtbl
{
    CObject_vtbl CObject;
};
struct CObList_mbrs
{
    CObList::CNode *m_pNodeHead;
    CObList::CNode *m_pNodeTail;
    int m_nCount;
    CObList::CNode *m_pNodeFree;
    struct CPlex *m_pBlocks;
    int m_nBlockSize;
};
struct CObList
{
    CObList_vtbl *__vftable;
    CObList_mbrs __members;
};

struct CPtrArray_vtbl
{
    CObject_vtbl CObject;
};
struct CPtrArray_mbrs
{
    void **m_pData;
    int m_nSize;
    int m_nMaxSize;
    int m_nGrowBy;
};
struct CPtrArray
{
    CPtrArray_vtbl *__vftable;
    CPtrArray_mbrs __members;
};

struct CRuntimeClass
{
	LPCSTR m_lpszClassName;
	int m_nObjectSize;
	UINT m_wSchema;
	CObject *(__stdcall *m_pfnCreateObject)();
#ifdef _AFXDLL
	CRuntimeClass *(__stdcall *m_pfnGetBaseClass)();
#else
    CRuntimeClass *m_pBaseClass;
#endif
	CRuntimeClass *m_pNextClass;
};

struct CStringData
{
	long nRefs;
	int nDataLength;
	int nAllocLength;
};
