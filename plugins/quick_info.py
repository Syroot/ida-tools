from ida_bytes import *
import idaapi


class QuickInfoPlugin(idaapi.plugin_t):
	flags = idaapi.PLUGIN_FIX
	comment = "Quick Info Plugin"
	help = "Shows quick info on the current byte"
	wanted_name = "QuickInfo"
	wanted_hotkey = "F12"

	def init(self):
		return idaapi.PLUGIN_KEEP

	def run(self, arg):
		ea = get_screen_ea()
		flags = get_full_flags(ea)
		# Print header
		flag_str = "{:032b}".format(flags)
		flag_parts = [flag_str[i:i+8] for i in range(0, len(flag_str), 8)]
		print("{:08X} flags are {}".format(ea, "_".join(flag_parts)))
		# Print original value
		print("{:>14}: 0x{:02X}".format("Value", flags & MS_VAL))
		# Print state
		print("{:>14}: {}".format("State", check_values(flags, {
			"TAIL": FF_TAIL,
			"DATA": FF_DATA,
			"CODE": FF_CODE
			}) or "UNK"))
		# Print state flags
		print("{:>14}: {}".format("State Flags", check_flags(flags, {
			"COMM": FF_COMM,
			"REF": FF_REF,
			"LINE": FF_LINE,
			"NAME": FF_NAME,
			"LABL": FF_LABL,
			"FLOW": FF_FLOW,
			"SIGN": FF_SIGN,
			"BNOT": FF_BNOT,
			"UNUSED": FF_UNUSED
			})))
		# Data or first & second operand
		first = check_values(flags, {
			"VOID": FF_0VOID,
			"NUMH": FF_0NUMH,
			"NUMD": FF_0NUMD,
			"CHAR": FF_0CHAR,
			"SEG": FF_0SEG,
			"OFF": FF_0OFF,
			"NUMB": FF_0NUMB,
			"NUMO": FF_0NUMO,
			"ENUM": FF_0ENUM,
			"FOP": FF_0FOP,
			"STRO": FF_0STRO,
			"STK": FF_0STK,
			"FLT": FF_0FLT,
			"CUST": FF_0CUST
			})
		second = check_values(flags, {
			"VOID": FF_1VOID,
			"NUMH": FF_1NUMH,
			"NUMD": FF_1NUMD,
			"CHAR": FF_1CHAR,
			"SEG": FF_1SEG,
			"OFF": FF_1OFF,
			"NUMB": FF_1NUMB,
			"NUMO": FF_1NUMO,
			"ENUM": FF_1ENUM,
			"FOP": FF_1FOP,
			"STRO": FF_1STRO,
			"STK": FF_1STK,
			"FLT": FF_1FLT,
			"CUST": FF_1CUST
			})
		print("{:>14}: {} / {}".format("1st / 2nd Op", first, second))
		# Data typing
		print("{:>14}: {}".format("Data Type", check_values(flags, {
			"BYTE": FF_BYTE,
			"WORD": FF_WORD,
			"DWORD": FF_DWORD,
			"QWORD": FF_QWORD,
			"TBYTE": FF_TBYTE,
			"STRLIT": FF_STRLIT,
			"STRUCT": FF_STRUCT,
			"OWORD": FF_OWORD,
			"FLOAT": FF_FLOAT,
			"DOUBLE": FF_DOUBLE,
			"PACKREAL": FF_PACKREAL,
			"ALIGN": FF_ALIGN,
			"erved": 0xC0000000,
			"CUSTOM": FF_CUSTOM,
			"YWORD": FF_YWORD,
			"ZWORD": FF_ZWORD
			})))

	def term(self):
		pass


def check_flags(flags, dict_flags):
	text = []
	for key, flag in dict_flags.iteritems():
		if flags & flag:
			text.append(key)
	return " | ".join(text)

def check_values(flags, dict_values):
	for key, value in dict_values.iteritems():
		if flags & value == value:
			return key


def PLUGIN_ENTRY():
	return QuickInfoPlugin()
