import idc
from ida_bytes import *
from ida_idaapi import *
from ida_kernwin import *
from ida_struct import *
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QCheckBox, QDialog, QGridLayout, QHBoxLayout,
                             QLabel, QLineEdit, QPushButton, QVBoxLayout)


class MsvcToolsPlugin(plugin_t):
	flags = PLUGIN_FIX
	comment = "MSVC Tools"
	help = "Utilities for reversing MSVC executables"
	wanted_name = "MSVC Tools"
	wanted_hotkey = ""

	def init(self):
		register_action(action_desc_t(
			CreateClassHandler.ID,
			"Add class...",
			CreateClassHandler(),
			"Alt+Ins",
			"Define a new class",
			0
		))
		attach_action_to_toolbar("StructuresToolBar", CreateClassHandler.ID)
		return PLUGIN_KEEP

	def run(self, arg):
		pass

	def term(self):
		pass


def PLUGIN_ENTRY():
	return MsvcToolsPlugin()



class CreateClassSettings(object):
	class_name = ""
	base_name = ""
	has_members = True

class CreateClassDialog(QDialog):
	def __init__(self, settings):
		super(CreateClassDialog, self).__init__(None, Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
		self.create_layout()
		# Apply settings.
		self.settings = settings
		self.le_class_name.setText(settings.class_name)
		self.le_base_name.setText(settings.base_name)
		self.cb_has_members.setChecked(settings.has_members)

	def create_layout(self):
		self.resize(300, 0)
		self.setWindowTitle("Create class")

		grid = QGridLayout()
		l_class_name = QLabel("Class name")
		self.le_class_name = QLineEdit()
		l_base_name = QLabel("Base name")
		self.le_base_name = QLineEdit()
		self.cb_has_members = QCheckBox("Has members")
		grid.addWidget(l_class_name, 0, 0)
		grid.addWidget(self.le_class_name, 0, 1)
		grid.addWidget(l_base_name, 1, 0)
		grid.addWidget(self.le_base_name, 1, 1)
		grid.addWidget(self.cb_has_members, 2, 1)

		hbl = QHBoxLayout()
		self.b_ok = QPushButton("OK")
		self.b_ok.clicked.connect(self.b_ok_clicked)
		b_cancel = QPushButton("Cancel")
		b_cancel.clicked.connect(self.b_cancel_clicked)
		hbl.addStretch(1)
		hbl.addWidget(self.b_ok)
		hbl.addWidget(b_cancel)
		hbl.addStretch(1)

		vbl = QVBoxLayout(self)
		vbl.addLayout(grid)
		vbl.addStretch(1)
		vbl.addLayout(hbl)

	def b_ok_clicked(self):
		# Transfer settings.
		self.settings.class_name = str(self.le_class_name.text())
		self.settings.base_name = str(self.le_base_name.text())
		self.settings.has_members = self.cb_has_members.isChecked()
		self.accept()

	def b_cancel_clicked(self):
		self.reject()


class CreateClassHandler(action_handler_t):
	ID = "msvctools:CreateClass"

	def __init__(self):
		action_handler_t.__init__(self)

	def activate(self, ctx):
		settings = CreateClassSettings()
		dialog = CreateClassDialog(settings)
		if dialog.exec_() and settings.class_name:
			# Create class struct.
			class_struct = add_struct(settings.class_name)
			# Create vftable struct.
			vtbl_name = settings.class_name + "_vtbl"
			vtbl_struct = add_struct(vtbl_name)
			if settings.base_name:
				add_member(vtbl_struct, 0, settings.base_name + "_vtbl", settings.base_name)
			# Create members struct. Needs at least 1 member to be referenced in another struct.
			mbrs_name = settings.class_name + "_mbrs"
			if settings.has_members:
				mbrs_struct = add_struct(settings.class_name + "_mbrs")
				if settings.base_name:
					add_member(mbrs_struct, 0, settings.base_name + "_mbrs", settings.base_name)
				else:
					add_member(mbrs_struct, 0, None, "dummy")
			# Add class fields.
			add_member(class_struct, 0, vtbl_name + "*", "__vftable")
			if settings.has_members:
				add_member(class_struct, 4, mbrs_name, "__members")
		return 1

	def update(self, ctx):
		return AST_ENABLE_ALWAYS

def add_struct(name):
	tid = add_struc(BADADDR, name)
	return get_struc(tid)

def add_member(struct, offset, type, name):
	add_struc_member(struct, name, offset, FF_DATA | FF_DWORD, None, 4)
	member = get_member(struct, offset)
	if type:
		idc.SetType(member.id, type)
	return member
