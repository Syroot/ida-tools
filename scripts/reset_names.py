import ida_idaapi
import ida_kernwin
import ida_name
import idautils


def execute():
    # Ask for text to find to reset.
    find_str = ida_kernwin.ask_str("", ida_kernwin.HIST_IDENT, "Text to find in names to reset them")
    if not find_str:
        return
    print(f"Names to reset which include '{find_str}':")

    # Find all occurrences of the text in names.
    names = []
    for ea, name in idautils.Names():
        if find_str in name:
            print(f"{ea:X}: {name}")
            names.append((ea, name))

    if not len(names):
        ida_kernwin.warning(f"Could not find any names containing '{find_str}'.")
        return

    # Ask for reset and execute if confirmed.
    max_listed = 10
    message = f"Do you want to reset the following {len(names)} names?"
    for ea, name in names[:max_listed]:
        message += f"\n{ea:08X}\t{name}"
    if len(names) > max_listed:
        message += f"\n(only {max_listed} names are previewed - see output window for a complete list)"

    resets = 0
    if ida_kernwin.ask_yn(ida_kernwin.ASKBTN_YES, message) == ida_kernwin.ASKBTN_YES:
        print(f"Reset names which included '{find_str}':")
        for ea, name in names:
            if ida_name.set_name(ea, "", 0):
                print(f"{ea:X}: {name}.")
                resets += 1
        ida_kernwin.info(f"Reset {resets} occurences.")


# Show the settings dialog without IDA displaying a blocking "Running Script" dialog.
timeout = ida_idaapi.set_script_timeout(0)
execute()
ida_idaapi.set_script_timeout(timeout)
