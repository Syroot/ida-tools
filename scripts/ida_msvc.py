import ida_bytes
import ida_name
import idaapi
import idc
import struct


class CompleteObjectLocator(object):
	_FORMAT = "iiiii"
	_SIZE = struct.calcsize(_FORMAT)

	def __init__(self, ea):
		self.ea = ea
		(
			self.signature,  # always 0
			self.offset,  # offset of this vftable in complete class (from top)
			self.cd_offset,  # offset to constructor displacement
			type_ea,  # address of TypeDescriptor of the complete class
			self._hierarchy_ea  # address of HierarchyDescriptor describing inheritance hierarchy
		) = struct.unpack(self._FORMAT, ida_bytes.get_bytes(ea, self._SIZE))
		self.type = TypeDescriptor(type_ea)
		if self.signature != 0 or self.offset < 0 or self.cd_offset < 0 or self._hierarchy_ea < 0:
			raise ValueError("Invalid CompleteObjectLocator.")

	@property
	def hierarchy(self):
		return HierarchyDescriptor(self._hierarchy_ea)


class TypeDescriptor(object):
	_FORMAT = "ii"
	_SIZE = struct.calcsize(_FORMAT)

	def __init__(self, ea):
		self.ea = ea
		(
			self.vftable_ea,  # address of RTTI's vftable
			self.runtime_ref  # internal runtime reference
		) = struct.unpack(self._FORMAT, ida_bytes.get_bytes(ea, self._SIZE))
		name_bytes = idc.get_strlit_contents(self.ea + 8)
		self.name = None if name_bytes is None else name_bytes.decode("utf-8")
		if self.vftable_ea < 0 or self.runtime_ref != 0 or not self.name or not self.name.startswith(".?A"):
			raise ValueError("Invalid TypeDescriptor.")


class HierarchyDescriptor(object):
	_FORMAT = "iiii"
	_SIZE = struct.calcsize(_FORMAT)

	def __init__(self, ea):
		self.ea = ea
		(
			self.signature,  # always 0
			self.attributes,  # bit 0 = multiple inheritance, bit 1 = virtual inheritance
			self.num_bases,  # number of BaseClassDescriptor in base class array
			self.bases_ea  # address of base class array
		) = struct.unpack(self._FORMAT, ida_bytes.get_bytes(ea, self._SIZE))

	@property
	def bases(self):
		ea = self.bases_ea
		for _ in range(self.num_bases):
			yield BaseDescriptor(ida_bytes.get_dword(ea))
			ea += 4

	@property
	def is_single_inheritance(self):
		return self.attributes & 0b11 == 0


class BaseDescriptor(object):
	_FORMAT = "iiiiii"
	_SIZE = struct.calcsize(_FORMAT)

	def __init__(self, ea):
		self.ea = ea
		(
			self._type_ea,  # address of TypeDescriptor of the class
			self.num_contained_bases,  # number of sub elements within base class array
			self.mdisp,  # member displacement
			self.pdisp,  # vftable displacement
			self.vdisp,  # displacement within vftable
			self.attributes  # base class attributes, usually 0
		) = struct.unpack(self._FORMAT, ida_bytes.get_bytes(ea, self._SIZE))

	@property
	def type(self):
		return TypeDescriptor(self._type_ea)


def demangle(name):
	orig_name = name
	try:
		if not name:
			return None
		elif name.startswith(".?A"):
			# Custom demangling of typeid(struct x) as used in RTTI type descriptors, abusing ctor demangling.
			ctor_name = "??0" + name[4:] + "QAE@XZ"
			name = ida_name.demangle_name(ctor_name, 0)
			# Remove ctor prefix and suffix.
			name = name[len("public: __thiscall ") : name.rfind("(")]
			# Remove ctor name.
			return "::".join(name.split("::")[:-1])
		else:
			# Use IDAs demangling logic after removing counter suffixes.
			name = remove_counter_suffix(name)
			return ida_name.demangle_name(name, 0)
	except:
		return orig_name

def remove_counter_suffix(name):
	suffix = name.rfind('_')
	if suffix != -1 and name[suffix + 1 :].isdigit():
		return name[:suffix]
	return name
