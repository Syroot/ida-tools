import idc

# Set colors here (BGR)
light_color = 0xEEFFF0  # default IDA green
dark_color = 0x223525  # dark green

for seg_ea in Segments():
    for func_ea in Functions(idc.get_segm_start(seg_ea), idc.get_segm_end(seg_ea)):
        func = idaapi.get_func(func_ea)
        if func.color == light_color:
            func.color = dark_color
        elif func.color == dark_color:
            func.color = light_color
