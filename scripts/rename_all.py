import ida_idaapi
import ida_kernwin
import ida_name
import idautils


def execute():
    # Ask for text to find and to replace with.
    old_str = ida_kernwin.ask_str("", ida_kernwin.HIST_IDENT, "Text to find in names")
    if not old_str:
        return
    new_str = ida_kernwin.ask_str("", ida_kernwin.HIST_IDENT, "Text to replace with")
    if not new_str:
        return
    print(f"Names found in which to replace '{old_str}' with '{new_str}':")

    # Find all occurrences of the text in names.
    names = []
    for ea, name in idautils.Names():
        if old_str in name:
            new_name = name.replace(old_str, new_str)
            print(f"{ea:X}: {name} -> {new_name}")
            names.append((ea, name))

    if not len(names):
        ida_kernwin.warning(f"Could not find any names containing '{old_str}'.")
        return

    # Ask for rename and execute if confirmed.
    max_listed = 10
    message = f"Do you want to update {len(names)} names as follows?"
    for ea, name in names[:max_listed]:
        new_name = name.replace(old_str, new_str)
        message += f"\n{ea:08X}\t{name} -> {new_name}"
    if len(names) > max_listed:
        message += f"\n(only {max_listed} names are previewed - see output window for a complete list)"

    renames = 0
    if ida_kernwin.ask_yn(ida_kernwin.ASKBTN_YES, message) == ida_kernwin.ASKBTN_YES:
        print(f"Updated names in which to replace '{old_str}' with '{new_str}':")
        for ea, name in names:
            new_name = name.replace(old_str, new_str)
            if ida_name.set_name(ea, new_name, 0):
                print(f"{ea:X}: {name} -> {new_name}.")
                renames += 1
        ida_kernwin.info(f"Renamed {renames} occurences.")


# Show the settings dialog without IDA displaying a blocking "Running Script" dialog.
timeout = ida_idaapi.set_script_timeout(0)
execute()
ida_idaapi.set_script_timeout(timeout)
