import idautils
import idc
from ida_bytes import *
from ida_funcs import *
from ida_idaapi import *
from ida_kernwin import *
from ida_lines import *
from ida_msvc import *
from ida_nalt import *
from ida_name import *
from ida_segment import *
from ida_struct import *
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QCheckBox, QDialog, QHBoxLayout, QLabel,
                             QPushButton, QVBoxLayout)

# ---- String Scanning -------------------------------------------------------------------------------------------------

'''
Scans the given segment (typically .rdata) for UTF-16 strings which IDA did not detect at all or incorrectly.

These are recognizable by a referenced offset with several "loc_123456(+x)" offsets following pointing at apparently
random positions, actually being the character code points:

.rdata:008F45C0 ; wchar_t off_8F45C0
.rdata:008F45C0 off_8F45C0 dd offset loc_65006F+1    ; DATA XREF: sub_4016D0+1D1
.rdata:008F45C4            dd offset loc_50006B+9
.rdata:008F45C8            dd offset sub_6E0061
.rdata:008F45CC            dd offset loc_6C0064+1
.rdata:008F45D0            db    0
.rdata:008F45D1            db    0
.rdata:008F45D2            db    0
.rdata:008F45D3            db    0

Sometimes these strings are detected only partly, with the start being offsets, but the end being proper UTF-16 strings.
This is especially confusing, as there are only references to the start, but not to the correctly detected part:

.rdata:008F4BD0 ; wchar_t off_8F4BD0
.rdata:008F4BD0 off_8F4BD0 dd offset byte_740069     ; DATA XREF: sub_40D650+12E
.rdata:008F4BD4            dd offset loc_6D0063+2
.rdata:008F4BD8 a01_1:
.rdata:008F4BD8     text "UTF-16LE", '01',0
.rdata:008F4BDE     align 10h

This script checks if every 4th byte in a segment is referenced by typical instructions, have only expected UTF-16
characters and a possible optional alignment to the next 4 bytes following. Then, the literal (and alignment) is set:

.rdata:008F45C0 ; wchar_t aPetpanel
.rdata:008F45C0 aPetpanel:                           ; DATA XREF: sub_4016D0+1D1
.rdata:008F45C0     text "UTF-16LE", 'petPanel',0
.rdata:008F45D2     align 4

.rdata:008F4BD0 ; wchar_t aItem01
.rdata:008F4BD0 aItem01:                             ; DATA XREF: sub_40D650+12E
.rdata:008F4BD0     text "UTF-16LE", 'item01',0
.rdata:008F4BDE     align 10h

If the script does not detect your strings, check the following functions:
- has_string_xref: requires offsets to be accessed by specific instructions
- is_valid_character: limits characters to specific UTF-16 code point ranges to prevent mojibake from being valid
'''

def scan_strings(seg):
	# Scan through the given segment.
	ea = seg.start_ea
	while ea < seg.end_ea:
		ea += scan_string_ea(ea)

def scan_string_ea(ea):
	'''
	Creates a string literal and optional 4-byte alignment if there is a valid UTF-16 string at ea.
	Returns the number of bytes processed.
	'''
	# Check if refereced by typical "push / mov offset" instructions.
	if not has_string_xref(ea):
		return 4
	# Require valid string and a succeeding optional 4-byte alignment.
	length = get_max_strlit_length(ea, STRTYPE_C_16, ALOPT_IGNHEADS | ALOPT_IGNCLT)
	if length == 0:
		return 4
	end = ea + length
	length_align = next_alignment(end) - end
	if length_align > 0 and any(x != 0 for x in get_bytes(end, length_align)):
		return 4
	# Check if valid unicode characters to prevent mojibake.
	strbytes = idc.get_strlit_contents(ea, length, STRTYPE_C_16)
	str = strbytes.decode("utf8")
	if not all(is_valid_character(c) for c in str):
		return 4
	# String detected, undefine existing bytes and create new string literal and alignment.
	if length_align > 0:
		print (f"{ea:08X} string \"{str}\" align {length_align}")
	else:
		print (f"{ea:08X} string \"{str}\"")
	for i in range(length):
		del_items(ea + i)
	create_strlit(ea, length, STRTYPE_C_16)
	create_align(end, length_align, length_align)
	# Return number of bytes processed for this string.
	return length + length_align

def has_string_xref(ea):
	for xref in idautils.XrefsTo(ea):
		# Check a simple offset.
		if get_dword(xref.frm) == ea:
			return True
		# Check push or mov opcodes.
		op = get_byte(xref.frm)
		if op == 0x68 or op == 0xB8:
			return get_dword(xref.frm + 1) == ea
		elif op == 0xC7:
			return ea in (get_dword(xref.frm + 3), get_dword(xref.frm + 6))
	return False

def next_alignment(ea, align=4):
	return (ea + align - 1) // align * align

def is_valid_character(char):
	# Currently allows ANSI, Hangul Compatibility Jamo, and pre-composed Hangul characters.
	code = ord(char)
	return 0x09 <= code <= 0x7E \
		or 0x3130 <= code <= 0x318E \
		or 0xAC00 <= code <= 0xD7A4

# ---- Vftable Scanning ------------------------------------------------------------------------------------------------

'''
Use RTTI to find all vftables in an MSVC executable and names them according to their class hierarchy.
Fixes IDA not detecting vftables or not naming them by their hierarchy (creating duplicate names).
'''

class Vtbl(object):
	def __init__(self, ea, text_seg):
		self.ea = ea
		# Get a valid RTTI Complete Object Locator (COL).
		col_ofs_ea = self.ea - 4
		self.col = CompleteObjectLocator(get_dword(col_ofs_ea))
		# Count the number of methods following the COL, requiring at least 1.
		self.num_methods = 0
		while True:
			# Check if the offset points to code and start of function.
			func_ea = get_dword(ea)
			if not text_seg.start_ea <= func_ea < text_seg.end_ea:
				break
			func = get_func(func_ea)
			if not func or func.start_ea != func_ea:
				break
			ea += 4
			self.num_methods += 1
		if self.num_methods == 0:
			raise ValueError("Vtbl has no methods.")
		# Detect base name.
		self.base_name = None
		for base in list(self.col.hierarchy.bases)[1:]:  # First is class itself
			if base.mdisp == self.col.offset:
				self.base_name = base.type.name
				break

	@property
	def class_name(self):
		return self.col.type.name

	@property
	def class_offset(self):
		return self.col.offset

def scan_vftables(rdata_seg, text_seg):
	# Check each dword in the rdata segment being the start of a vftable.
	class_dict = {}  # Dict[string (vtbl class name), List[Vtbl]]
	ea = rdata_seg.start_ea + 4
	while ea < rdata_seg.end_ea:
		try:
			# Try parsing a vftable from ea.
			vtbl = Vtbl(ea, text_seg)
			# Append it to a list of vftables with the same class.
			class_vtbls = class_dict.get(vtbl.class_name)
			if class_vtbls is None:
				class_dict[vtbl.class_name] = [vtbl]
			else:
				add_vtbl_to_class(class_vtbls, vtbl)
			# Generate names for vftable and complete object locator.
			if vtbl.class_offset == 0:
				suffix = f"{vtbl.class_name[4:]}6B@"
			else:
				suffix = f"{vtbl.class_name[4:]}6B{vtbl.base_name[4:]}@"
			vtbl_name = "??_7" + suffix
			col_name = "??_R4" + suffix
			if vtbl.class_offset == 0:
				print(f"{vtbl.ea:08X} {demangle(vtbl_name)}, base {demangle(vtbl.base_name)} ({vtbl.num_methods} methods)")
			else:
				print(f"{vtbl.ea:08X} {demangle(vtbl_name)} ({vtbl.num_methods} methods)")
			# Recreate existing comments to prevent duplicate ones. Note that IDA still creates them after first run.
			create_col_comments(vtbl.col)
			# Set offsets and names.
			create_dword(ea - 4, 4)  # Ensure COL offset.
			base_terminator_ea = vtbl.col.hierarchy.bases_ea + 4 * vtbl.col.hierarchy.num_bases
			create_dword(base_terminator_ea, 4)  # Ensure 0 terminator item.
			set_cmt(base_terminator_ea, "base class array terminator", False)
			ida_name.set_name(vtbl.ea, vtbl_name)
			ida_name.set_name(vtbl.col.ea, col_name)
			ea += vtbl.num_methods * 4
		except:
			ea += 4
	return class_dict

def add_vtbl_to_class(class_vtbls, vtbl):
	# Add a vtbl in order of its class offset, and only if the offset does not exist yet.
	idx = 0
	for idx, v in enumerate(class_vtbls):
		if vtbl.class_offset == v.class_offset:
			raise ValueError(f"Vtbl at offset {v.class_offset} already exists.")
		elif vtbl.class_offset < v.class_offset:
			break
	class_vtbls.insert(idx, vtbl)

# ---- Complete Object Locator ----

def create_col_comments(col):
	# Complete Object Locator
	set_cmt(col.ea, "signature", False)
	set_cmt(col.ea + 4, "offset of this vtable in complete class (from top)", False)
	set_cmt(col.ea + 8, "offset of constructor displacement", False)
	set_cmt(col.ea + 12, "reference to type description", False)
	set_cmt(col.ea + 16, "reference to hierarchy description", False)
	# Type Descriptor
	delete_extra_cmts(col.type.ea, idaapi.E_PREV)
	for line in get_col_hierarchy_cmt(col):
		add_extra_cmt(col.type.ea, True, line)
	set_cmt(col.type.ea, "reference to RTTI's vftable", False)
	set_cmt(col.type.ea + 4, "internal runtime reference", False)
	set_cmt(col.type.ea + 8, "type descriptor name", True)
	# Class Hierarchy Descriptor
	set_cmt(col.hierarchy.ea, "signature", False)
	set_cmt(col.hierarchy.ea + 4, "attributes", False)
	set_cmt(col.hierarchy.ea + 8, "# of items in the array of base classes", False)
	set_cmt(col.hierarchy.ea + 12, "reference to the array of base classes", False)
	# Base Class Descriptor array
	for i in range(col.hierarchy.num_bases):
		set_cmt(col.hierarchy.bases_ea + i * 4, f"reference to base class decription {i + 1}", False)
	# Base Class Descriptors
	for base in col.hierarchy.bases:
		set_cmt(base.ea, "reference to type description", False)
		set_cmt(base.ea + 4, "# of sub elements within base class array", False)
		set_cmt(base.ea + 8, "member displacement", False)
		set_cmt(base.ea + 12, "vftable displacement", False)
		set_cmt(base.ea + 16, "displacement within vftable", False)
		set_cmt(base.ea + 20, "base class attributes", False)
		set_cmt(base.ea + 24, "reference to class hierarchy descriptor", False)

def get_col_hierarchy_cmt(col):
	# Creates indented inheritance hierarchy comment. Omits ":" and "," separators for simplicity.
	def comment_vftable(bases, lines, indent):
		# Write current line.
		base = bases[0]
		lines.append("  " * indent + f"public struct {demangle(base.type.name)} /* mdisp:{base.mdisp} */")
		# Write all sub levels.
		bases_written = 0
		while base.num_contained_bases - bases_written > 0:
			bases_written += comment_vftable(bases[bases_written + 1 :], lines, indent + 1)
		return bases_written + 1

	lines = []
	comment_vftable(list(col.hierarchy.bases), lines, 0)
	return lines

# ---- Method Renaming -------------------------------------------------------------------------------------------------

def name_class_methods(class_dict, class_vtbls):
	def name_vtbl_methods(vtbl):
		# Check parents first.
		if vtbl.base_name:
			name_class_methods(class_dict, class_dict.get(vtbl.base_name))
		# Rename methods which do not have a name yet.
		class_name = demangle(vtbl.class_name) or vtbl.class_name
		ofs_ea = vtbl.ea
		for _ in range(vtbl.num_methods):
			create_dword(ofs_ea, 4, True)
			ea = get_dword(ofs_ea)
			name = ida_name.get_name(ea)
			if name.startswith("sub_"):
				new_name = f"{class_name}::{name}"
				new_name = ida_name.validate_name(new_name, ida_name.VNT_IDENT)
				ida_name.set_name(ea, new_name, 0)
			ofs_ea += 4

	if class_vtbls is None:
		return
	for vtbl in class_vtbls:
		name_vtbl_methods(vtbl)

# ---- Class Struct Creation -------------------------------------------------------------------------------------------

'''
This will create structs according to the vftable information given in the RTTI class hierarchy. It does not reserve
space for members though (the user has to decide whether he prefers member structs, or filling the gaps manually).

Example of a simple class without inheritance and one virtual method:

struct RefCounted // class RefCounted
{
  RefCounted *__vftable;
};
struct RefCounted_vtbl
{
  int vsub_0;
};

A class inheriting from this, providing 4 virtual methods in total (1 inherited), would look like:

struct Object // class Object : public RefCounted
{
  Object_vtbl *__vftable;
};
struct Object_vtbl
{
  RefCounted_vtbl RefCounted;
  int vsub_4;
  int vsub_8;
  int vsub_C;
};

In case of multiple inheritance, the vftables are created at the correct offsets. The resulting gaps store class data
(members). These are not created by the script, since the total size of members is often unknown and because the user
may want to create custom structs to store them in.

struct Window // class Window : public Object, IUIElement, IResizable
{
  Window_vtbl *__vftable;
  _BYTE gap4[8];                         // Object & RefCounted members (8 bytes)
  IUIElement_vtbl *__vftable_IUIElement; // 1st interface
  _BYTE gap10[12];                       // 1st interface members (12 bytes)
  IResizable_vtbl *__vftable_IResizable; // 2nd interface
                                         // 2nd interface members (size unknown)
                                         // Window members (size unknown)
};
struct Window_vtbl
{
  Object_vtbl Object;
  int vsub_20;
  int vsub_24;
  int vsub_28;
  int vsub_2C;
};
'''

def create_class_structs(class_dict, c):
	if c is None:  # Some weird interfaces are listed but don't seem to have a vftable.
		return
	main_vtbl = c[0]
	# Check if the structs for this class were already created.
	class_name = demangle(main_vtbl.class_name)
	if get_struc_id(class_name) != idaapi.BADADDR:
		return
	# Ensure base classes are created first.
	for vtbl in c:
		if vtbl.base_name:
			create_class_structs(class_dict, class_dict.get(vtbl.base_name))
	# Create structs for this class.
	print(f"Creating {class_name} structs")
	class_struct = add_struct(class_name)
	for vtbl in c:
		if vtbl == main_vtbl:
			# Create main vftable struct.
			vtbl_name = demangle(vtbl.class_name) + "_vtbl"
			vtbl_struct = add_struct(vtbl_name)
			num_methods_base = 0
			base_vtbl = None if not vtbl.base_name else class_dict.get(vtbl.base_name)
			if base_vtbl:
				num_methods_base = base_vtbl[0].num_methods
				base_name = demangle(base_vtbl[0].class_name)
				add_member(vtbl_struct, 0, base_name + "_vtbl", base_name)
			for i in range(num_methods_base, vtbl.num_methods):
				add_member(vtbl_struct, i * 4, None, "vsub_{:X}".format(i * 4))
			# Add class members.
			add_member(class_struct, 0, vtbl_name + "*", "__vftable")
		else:
			# Create sub vftable members.
			base_name = demangle(vtbl.base_name)
			add_member(class_struct, vtbl.class_offset, base_name + "_vtbl*", "__vftable_" + base_name)

def add_struct(name):
	tid = add_struc(BADADDR, name)
	return get_struc(tid)

def add_member(struct, offset, type, name):
	add_struc_member(struct, name, offset, FF_DATA | FF_DWORD, None, 4)
	member = get_member(struct, offset)
	if type and member:
		idc.SetType(member.id, type)
	return member

# ---- Settings and UI -------------------------------------------------------------------------------------------------

class Settings(object):
	scan_strings = True
	scan_vftables = True
	rename_methods = True
	create_structs = True

class SettingsDialog(QDialog):
	def __init__(self):
		super(SettingsDialog, self).__init__(None, Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
		self.setFixedSize(250, 0)
		self.setWindowTitle("MSVC Scanner")

		self.cb_scan_strings = QCheckBox("Scan for UTF-16 strings")
		self.cb_scan_strings.setChecked(True)
		self.cb_scan_vftables = QCheckBox("Scan for vftables")
		self.cb_scan_vftables.setChecked(True)
		self.cb_scan_vftables.stateChanged.connect(self._cb_scan_vftables_state_changed)
		self.cb_rename_methods = QCheckBox("Rename vftable methods")
		self.cb_rename_methods.setChecked(True)
		self.cb_create_structs = QCheckBox("Create class structs")
		self.cb_create_structs.setChecked(True)

		b_ok = QPushButton("Start Scan")
		b_ok.clicked.connect(self._b_ok_clicked)
		b_cancel = QPushButton("Cancel")
		b_cancel.clicked.connect(self._b_cancel_clicked)

		hbl = QHBoxLayout()
		hbl.addStretch(1)
		hbl.addWidget(b_ok)
		hbl.addWidget(b_cancel)

		vbl = QVBoxLayout(self)
		vbl.addWidget(QLabel("Please choose the tasks to run:"))
		vbl.addWidget(self.cb_scan_strings)
		vbl.addWidget(self.cb_scan_vftables)
		vbl.addWidget(self.cb_rename_methods)
		vbl.addWidget(self.cb_create_structs)
		vbl.addStretch(1)
		vbl.addLayout(hbl)

	def get_settings(self):
		settings = Settings()
		settings.scan_strings = self.cb_scan_strings.isChecked()
		settings.scan_vftables = self.cb_scan_vftables.isChecked()
		settings.rename_methods = self.cb_rename_methods.isChecked()
		settings.create_structs = self.cb_create_structs.isChecked()
		return settings

	def _cb_scan_vftables_state_changed(self, state):
		scan_vftables = state == Qt.Checked
		self.cb_rename_methods.setEnabled(scan_vftables)
		self.cb_rename_methods.setCheckState(Qt.Checked if scan_vftables else Qt.Unchecked)
		self.cb_create_structs.setEnabled(scan_vftables)
		self.cb_create_structs.setCheckState(Qt.Checked if scan_vftables else Qt.Unchecked)

	def _b_ok_clicked(self):
		self.accept()

	def _b_cancel_clicked(self):
		self.reject()

def get_settings():
	settings = None
	# Show the settings dialog without IDA kicking in with a blocking "Running Script" dialog.
	timeout = set_script_timeout(0)
	dialog = SettingsDialog()
	if dialog.exec_():
		settings = dialog.get_settings()
	set_script_timeout(timeout)
	return settings

def run(settings):
	data_seg = get_segm_by_name(".rdata") or get_segm_by_name(".data")
	if not data_seg:
		warning("Data segment not found. Ensure to have a segment named either .rdata or .data.")
		return
	text_seg = get_segm_by_name(".text")
	if not text_seg:
		warning("Code segment not found. Ensure to have a segment named .text.")
		return

	if settings.scan_strings:
		scan_strings(data_seg)

	if settings.scan_vftables:
		class_dict = scan_vftables(data_seg, text_seg)
		# Name vftable methods and create structs.
		for c in iter(class_dict.values()):
			if settings.rename_methods:
				name_class_methods(class_dict, c)
			if settings.create_structs:
				create_class_structs(class_dict, c)

	print("Done.")

settings = get_settings()
if settings:
	run(settings)
