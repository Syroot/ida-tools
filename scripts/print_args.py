from ida_bytes import *
from ida_kernwin import *
from ida_funcs import *
from idautils import *
import idaapi
import idc

for xref in XrefsTo(get_screen_ea(), 0):
	call_ea = xref.frm
	args_ea = idaapi.get_arg_addrs(call_ea)
	if args_ea:
		print("0x{:08X} {}".format(call_ea, idc.GetDisasm(args_ea[-1])))
	else:
		print("0x{:08X} no args recognized".format(call_ea))
