# IDA Tools

This repository is a dump of IDA 7 definitions, Python scripts and plugins which may be useful to you.

## Definitions

Copy the contents of the following files in the `defs` folder and paste them to the Structures window in IDA to extend
its understanding of the corresponding libraries:
- `mfc4`: Base definitions for Microsoft Foundation Classes 4.

## Plugins

Install the following scripts to the plugin folder to extend the functionality of IDA:
- `msvc_tools`: Plugin allowing quick creation of structs for C++ classes.
- `quick_info`: Plugin which prints flag information on the current byte when F12 is pressed.

## Scripts

Run the following files as a script for one-time scans or functionality:
- `change_decompiled_func_color`: Replace the color set to decompiled functions in case you switch your color scheme.
- `msvc_scanner`: Script which finds _all_ UTF-16 strings and vftables in an MSVC executable correctly, properly names
  vftable addresses and methods and creates structs according to their class hierarchy and layout. Most prominently
  fixes IDA's stupid duplicate names for vftables in case of multiple inheritance.
- `print_args`: Prints arguments in all calls to the current function (very specific and raw, may need your adjustment).
- `rename_funcs`: Searches and replaces text in all function names.

More information is given in each file.

## Themes

IDA 7.3-compatible themes, which simulate the design of other programs. They may be slightly incomplete, please let me
know about bugs or enhancement ideas, as I don't know how to re-design specific areas of IDA properly yet.
- `vs2019-dark`: Visual Studio 2019 Dark theme, very close replication, except for tabs which must look like file tabs.
  ![](themes/vs2019-dark/preview.png)
- `vscode-dark`: Visual Studio Code Dark theme, enhanced IDASkins version with tiny / tidy scrollbars and splitters.
  ![](themes/vscode-dark/preview.png)
